<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

$logged_user = wp_get_current_user();
?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<p>Logged in as: <?php echo $logged_user->display_name; ?></p>
<p> Posts dates  by the user:</p>
<?php
foreach($this->current_user_posts as $post){
echo $post->post_date."<br>";
}
?>

<h3><?php /*echo $this->testHelloWorld->display_name;*/ ?></h3>
<?php 
// default Yii generated codes ...
/*
<p>Congratulations! You have successfully created your Yii application.</p>

<p>You may change the content of this page by modifying the following two files:</p>
<ul>
	<li>View file: <code><?php echo __FILE__; ?></code></li>
	<li>Layout file: <code><?php echo $this->getLayoutFile('main'); ?></code></li>
</ul>

<p>For more details on how to further develop this application, please read
the <a href="http://www.yiiframework.com/doc/">documentation</a>.
Feel free to ask in the <a href="http://www.yiiframework.com/forum/">forum</a>,
should you have any questions.</p>
*/?>

