<?php
/* @var $this WpUsersController */
/* @var $model WpUsers */

$this->breadcrumbs=array(
	'Wp Users'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List WpUsers', 'url'=>array('index')),
	array('label'=>'Create WpUsers', 'url'=>array('create')),
	array('label'=>'Update WpUsers', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete WpUsers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage WpUsers', 'url'=>array('admin')),
);
?>

<h1>View WpUsers #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'user_login',
		/*'user_pass',*/
		'user_nicename',
		'user_email',
		'user_url',
		'user_registered',
		'user_activation_key',
		'user_status',
		'display_name',
	),
)); ?>
