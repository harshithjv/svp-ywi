<?php
/* @var $this WpUsersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Wp Users',
);

$this->menu=array(
	array('label'=>'Create WpUsers', 'url'=>array('create')),
	array('label'=>'Manage WpUsers', 'url'=>array('admin')),
);
?>

<h1>Wp Users</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
