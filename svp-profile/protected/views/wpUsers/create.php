<?php
/* @var $this WpUsersController */
/* @var $model WpUsers */

$this->breadcrumbs=array(
	'Wp Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List WpUsers', 'url'=>array('index')),
	array('label'=>'Manage WpUsers', 'url'=>array('admin')),
);
?>

<h1>Create WpUsers</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>