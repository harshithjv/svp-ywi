<?php
/* @var $this WpUsersController */
/* @var $model WpUsers */

$this->breadcrumbs=array(
	'Wp Users'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List WpUsers', 'url'=>array('index')),
	array('label'=>'Create WpUsers', 'url'=>array('create')),
	array('label'=>'View WpUsers', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage WpUsers', 'url'=>array('admin')),
);
?>

<h1>Update WpUsers <?php echo $model->ID; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>